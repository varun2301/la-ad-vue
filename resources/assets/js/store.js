import Vue from 'vue';
import Vuex from 'vuex';

import * as getters from './store/getters';
import * as mutations from './store/mutations';
import * as actions from './store/actions';

import user from './store/modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
    strict: true,

    modules: {
        user,
    },
    getters,
    mutations,
    actions
});