require('./bootstrap');

import VueRouter from 'vue-router';
import routes from './routes.js';
//import store from './store';

//Vue.component('master-component',require('./pages/Admin.vue'));

const router = new VueRouter({
    mode: 'history',
    routes
});

new Vue({
    el: '#app',
    router,
    //store,
    
});
