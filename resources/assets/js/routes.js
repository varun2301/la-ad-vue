/*import VueRouter from 'vue-router';




var router = new VueRouter({

  routes: [
    { path: '/foo', component: Admin },
    { path: '/bar', component: Admin },
    {
		path: '/home',
		component: Admin,
	}
  ]
})

export default router;*/

import Admin from './pages/Admin.vue';
import User from './pages/User.vue';


let routes = [
	{
		path : '/la-ad-vue/public/home',
		component : Admin,
	},
	{
		path : '/la-ad-vue/public/user',
		component : User,
	}
];

export default routes;