<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>  here </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{asset('/css/app.css')}}" rel="stylesheet" type="text/css" />    
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
       <!--  <script>
            $(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });
        </script> -->
    </head>

    <body>
        <div id="app">
            <router-view></router-view>
            <!-- <master-component></master-component> -->
        </div>
        
        <script src="{{asset('/js/app.js')}}" type="text/javascript"></script>
    </body>
</html>
